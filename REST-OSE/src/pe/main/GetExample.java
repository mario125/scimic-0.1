package pe.main;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class GetExample {

	public static void main(String[] args) throws ClientProtocolException, IOException {
		// TODO Auto-generated method stub
		String GET_URL = "http://localhost:8080/ose-web-server/api/user/8";
		HttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(GET_URL);
		HttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		String jsonResponse = entity != null ? EntityUtils.toString(entity) : null;
		System.out.println(jsonResponse);
	}

}
